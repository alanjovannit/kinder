package Métodos;

public class pagos{
    int id; //primary key
    String curp;
    int dinero;
    String fecha;

    public pagos(int id, String curp, int dinero, String fecha) {
        this.id = id;
        this.curp = curp;
        this.dinero = dinero;
        this.fecha = fecha;
    }
    
    public pagos(int dinero, String fecha){
        this.dinero=dinero;
        this.fecha=fecha;
    }
    
    public pagos(int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public int getDinero() {
        return dinero;
    }

    public void setDinero(int dinero) {
        this.dinero = dinero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

     @Override
    public String toString(){
        return id +"\t"+ curp +"\t"+ dinero+"\t"+fecha;
    } 
    
}
