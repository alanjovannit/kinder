package Métodos;

public class tutor {
    private int id;
    private String nombre, curp;

    public tutor(String nombre, String curp) {
        this.nombre = nombre;
        this.curp = curp;
    }
    
    public tutor(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }
    
    
    
}
