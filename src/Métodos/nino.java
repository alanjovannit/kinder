package Métodos;

public class nino {
    private String nombre, curp, cp;
    private String nacimiento;
    private String elemento;

    public nino(String curp, String nombre, String cp, String nacimiento) {
        this.nombre = nombre;
        this.curp = curp; //Primary KEY
        this.cp = cp;
        this.nacimiento = nacimiento;
    }
    
    public nino(String nombre, String curp){
        this.nombre=nombre;
        this.curp=curp;
    }
    
    public nino(String elemento){
        this.elemento=elemento;
    }

    public String getElemento() {
        return elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }
    
    @Override
    public String toString(){
        return nombre +"\t"+ curp +"\t"+ cp+"\t"+nacimiento;
    }
    
    public String toString2(){
        return nombre+"\t"+ curp;
    }
}
