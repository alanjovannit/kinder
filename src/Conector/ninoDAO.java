package Conector;

import Métodos.nino;
import Métodos.pagos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
 import java.util.Random;
import javax.swing.JLabel;

public class ninoDAO implements DAOGenerico<nino, String>{
    static final String DATABASE_URL="jdbc:sqlite:nino.db";
    private Connection conexion;
    
    public ninoDAO() throws SQLException{
        conexion=DriverManager.getConnection(DATABASE_URL);
    }

    @Override
    public void insertar(nino elemento) throws SQLException {
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("insert into nino values ('"+elemento.getCurp()+"', '"+elemento.getNombre()+"', '"+elemento.getCp()+"', '"+elemento.getNacimiento()+"')");
        stmt.close();
    }
    
    @Override
    public void eliminar(nino elemento) throws SQLException{
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("delete from nino where curp= '"+elemento.getCurp()+"'");
        stmt.close();
    }

    @Override
    public nino get(String curp) throws SQLException {
        Statement stmt=conexion.createStatement();
        ResultSet rs= stmt.executeQuery("select *"+" from nino"+" where nombre like '"+curp+"'");
        rs.next();
        nino nino = new nino(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
        rs.close();
        stmt.close();
        return nino;
    }
    
    public List<nino> busqueda(String bus) throws SQLException{
        List<nino> ninos;
        try (Statement stmt = conexion.createStatement()){
            ninos = new ArrayList<>();
            ResultSet rs=stmt.executeQuery("select curp, nombre, cp, nacimiento from nino where nombre like '%"+bus+"%'"+" or curp like '%"+bus+"%'");
            while(rs.next()){
            nino nino = new nino(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            ninos.add(nino);
            } 
        rs.close();
        stmt.close();
        }
        return ninos;
    }
    
    public List<nino> listar() throws SQLException {
        List<nino> ninos;
        try (Statement stmt = conexion.createStatement()) {
            ninos = new ArrayList<>();
            ResultSet rs =
                    stmt.executeQuery(
                            "select nombre, curp, Cp, nacimiento"
                                    + " from nino"
                    );
            while(rs.next()) {
                nino nino = new nino(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4));
                
                ninos.add(nino);
        }   rs.close();
        }
        return ninos;
    }
    
    public List<pagos> listarpa() throws SQLException{
        List<pagos> ninos;
        try (Statement stmt = conexion.createStatement()) {
            ninos = new ArrayList<>();
            ResultSet rs =
                    stmt.executeQuery(
                            "select id, curp, dinero, fecha"
                                    + " from pagos"
                    );
            while(rs.next()) {
                pagos pagos = new pagos(rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getString(4));
                
                ninos.add(pagos);
        }   rs.close();
        }
        return ninos;
    }
    
    public void actualizar(nino nino) throws SQLException {
        Statement stmt = conexion.createStatement();
        
        stmt.executeUpdate(
                        "update nino\n" +
                        "set nombre = '" + nino.getNombre()
                                + "',\n" +
                        "    nacimiento = '" + nino.getNacimiento() + "'\n" +
                        "    cp = '" + nino.getCp() +
                        "where curp = " + nino.getCurp()
                );
        
        stmt.close();
        
    }
     
    public void cerrarConexion() throws SQLException {
        conexion.close();
    }
    

    public String get(nino nino) throws SQLException {
         Statement stmt=conexion.createStatement();
         ResultSet rs= stmt.executeQuery("select nombre, curp, cp, nacimiento"+" from nino"+" where nombre = "+nino.getNombre());
         rs.next();
         nino nino1 = new nino(rs.getString(1));
         rs.close();
         stmt.close();
         return nino1.getElemento();
    }
    
    public void abonar(String elemento, int dinero, String fecha) throws SQLException {
        Statement stmt=conexion.createStatement();
        ninoDAO dao=new ninoDAO();
        stmt.executeUpdate("insert into pagos values ('"+dao.generarFolio()+"', '"+elemento+"', '"+dinero+"', '"+fecha+"')");
        stmt.close();
    }
    
     public void eliminarAbono(int id) throws SQLException {
        Statement stmt=conexion.createStatement();
        stmt.executeUpdate("delete from pagos where id = '"+id+"'");
        stmt.close();
    }
     
    public int generarFolio() throws SQLException {
        List<pagos> ninos;
        Statement stmt=conexion.createStatement();
        ninos=new ArrayList<>();
        int i=0;
        double z=Math.random();
        if(z<0.099999999){
           i=(int)(100000*Math.random());
        }else{
        i=(int)(10000*Math.random());}
        ResultSet rs=stmt.executeQuery("select id from pagos");
        while(rs.next()){
            pagos pago=new pagos(rs.getInt(1));
            ninos.add(pago);
        }
        for(int j=0;j<ninos.size();j++){
            if (i==ninos.get(j).getId()){
                i--;
            }
        }
        return i;
    }
     
    public List<pagos> busquedaPagos(String bus) throws SQLException{
        List<pagos> ninos;
        try (Statement stmt = conexion.createStatement()){
            ninos = new ArrayList<>();
            ResultSet rs=stmt.executeQuery("select id, curp, dinero, fecha from pagos where curp = '"+bus+"'");
            while(rs.next()){
            pagos pago = new pagos(rs.getInt(1), rs.getString(2),rs.getInt(3),rs.getString(4));
            ninos.add(pago);
            } 
        rs.close();
        stmt.close();
        }
        return ninos;
    }

    
}
