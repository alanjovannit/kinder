package Conector;

import java.sql.SQLException;

public interface DAOGenerico<E,K> {
    void insertar (E elemento) throws SQLException;
    void eliminar(E elemento) throws SQLException;
    E get(K llave) throws SQLException;
    void actualizar(E elemento) throws SQLException;
    
}
